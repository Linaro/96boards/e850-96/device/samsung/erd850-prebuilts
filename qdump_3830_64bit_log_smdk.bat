@echo off

setlocal

set date2=%date:-=%
set time2=%time: =0%
set time3=%time2:~0,2%%time2:~3,2%

echo on

md %date2%_%time3%

qdump64 ramdump f0010000 1fffff %date2%_%time3%\kernel.log
qdump64 ramdump f0210000 3fffff %date2%_%time3%\platform.log
rem qdump64 ramdump f9610000 3fffff %date2%_%time3%\sfr.log

endlocal