fastboot flash fwbl1 fwbl1.img
fastboot flash bl2 bl2.img
fastboot flash el3_mon el3_mon.img
fastboot flash epbl epbl.img
fastboot flash bootloader bootloader.img

fastboot flash tzsw tzsw.img
fastboot flash ldfw ldfw.img

set ANDROID_PRODUCT_OUT=%cd%

fastboot --skip-reboot flashall

fastboot reboot bootloader

fastboot flash modem modem.img

fastboot reboot -w
fastboot reboot
