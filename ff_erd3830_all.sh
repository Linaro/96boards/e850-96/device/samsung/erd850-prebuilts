#!/bin/bash

set -e

fastboot flash gpt		gpt.img
fastboot flash fwbl1		fwbl1.img
fastboot flash epbl		epbl.img
fastboot flash bl2		bl2.img
fastboot flash bootloader	bootloader.img
fastboot flash el3_mon		el3_mon.img

fastboot flash keystorage	keystorage.img
fastboot flash ldfw		ldfw.img
fastboot flash tzsw		tzsw.img

# Flash vbmeta.img only if AVB 2.0 is enabled
if [ -f vbmeta.img ]; then
	fastboot flash vbmeta	vbmeta.img
else
	fastboot erase vbmeta
fi

fastboot flash boot		boot.img
fastboot flash recovery		recovery.img
fastboot flash dtbo		dtbo.img
fastboot flash dtb		dtb.img

fastboot flash super		super.img -S 512M

fastboot format efs
fastboot format metadata
fastboot format persist
fastboot erase modem
fastboot flash modem		modem.bin
fastboot flash logo		logo.bin

fastboot -w

fastboot reboot
